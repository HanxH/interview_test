import { merge } from '../src/merge';

describe('merge', () => {
  it('two sorted arrays', () => {
    const collection_1 = [1, 3, 5];
    const collection_2 = [2, 4, 6];
    const expected = [1, 2, 3, 4, 5, 6];

    const result = merge(collection_1, collection_2);

    expect(result).toEqual(expected);
  });

  it('empty arrays', () => {
    const collection_1: number[] = [];
    const collection_2 = [1, 2, 3];
    const expected = [1, 2, 3];

    const result = merge(collection_1, collection_2);

    expect(result).toEqual(expected);
  });

  it(' different lengths', () => {
    const collection_1 = [1, 2, 3];
    const collection_2 = [4, 5];
    const expected = [1, 2, 3, 4, 5];

    const result = merge(collection_1, collection_2);

    expect(result).toEqual(expected);
  });
});
